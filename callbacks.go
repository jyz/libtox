/* Module      : callbacks.go
 * Description : Utility functions for registering callbacks.
 * Copyright   : Copyright (c) 2015 Vaurum Labs Inc. All rights reserved.
 * License     : GPLv3
 * Maintainer  : Dominic Williams <dominic@mirrorx.com>, Enzo Haussecker <enzo@mirrorx.com>
 * Stability   : Experimental
 * Portability : Non-portable (requires Tox core at commit fc54980)
 *
 * Tox instances handle events using callback functions. Only one callback can
 * be registered per event, so if a client needs multiple event listeners, then
 * it needs to implement the dispatch functionality itself. This module only
 * provides the hooks for registering them.
 */

package libtox

// #include <memory.h>
// #include <tox/tox.h>
import "C"
import "unsafe"

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// CALLBACK HOOKS ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//export callback_self_connection_status
func callback_self_connection_status(
    c_tox_ptr          *C.Tox,
    c_connection_status C.TOX_CONNECTION,
    c_user_data         unsafe.Pointer,
) {
    // Initialize variables.
    tox := (*Tox)(c_user_data)
    var connectionStatus ToxConnectionStatus
    switch c_connection_status {
        case C.TOX_CONNECTION_NONE:
            connectionStatus = ToxConnectionNone
        case C.TOX_CONNECTION_TCP:
            connectionStatus = ToxConnectionTCP
        case C.TOX_CONNECTION_UDP:
            connectionStatus = ToxConnectionUDP
    }
    // Register the callback.
    tox.onConnectionStatus(tox, connectionStatus)
}

//export callback_friend_name
func callback_friend_name(
    c_tox          *C.Tox,
    c_friend_number C.uint32_t,
    c_name         *C.uint8_t,
    c_length        C.size_t,
    c_user_data     unsafe.Pointer,
) {
    // Initialize variables.
    tox := (*Tox)(c_user_data)
    friendNumber := uint32(c_friend_number)
    name := make([]byte, c_length)
    // Copy the name.
    if (c_length > 0) {
        C.memcpy(
            unsafe.Pointer(&name[0]),
            unsafe.Pointer(c_name),
            c_length,
        )
    }
    // Register the callback.
    tox.onFriendName(tox, friendNumber, name)
}

//export callback_friend_request
func callback_friend_request(
    c_tox        *C.Tox,
    c_public_key *C.uint8_t,
    c_message    *C.uint8_t,
    c_length      C.size_t,
    c_user_data   unsafe.Pointer,
) {
    // Initialize variables.
    tox := (*Tox)(c_user_data)
    var publicKey ToxPublicKey
    message := make([]byte, c_length)
    // Copy the public key.
    C.memcpy(
        unsafe.Pointer(&publicKey[0]),
        unsafe.Pointer(c_public_key),
        ToxPublicKeySize,
    )
    // Copy the friend request message.
    if (c_length > 0) {
        C.memcpy(
            unsafe.Pointer(&message[0]),
            unsafe.Pointer(c_message),
            c_length,
        )
    }
    // Register the callback.
    tox.onFriendRequest(tox, publicKey, message)
}

//export callback_friend_status_message
func callback_friend_status_message(
    c_tox          *C.Tox,
    c_friend_number C.uint32_t,
    c_message      *C.uint8_t,
    c_length        C.size_t,
    c_user_data     unsafe.Pointer,
) {
    // Initialize variables.
    tox := (*Tox)(c_user_data)
    friendNumber := uint32(c_friend_number)
    message := make([]byte, c_length)
    // Copy the status message.
    if (c_length > 0) {
        C.memcpy(
            unsafe.Pointer(&message[0]),
            unsafe.Pointer(c_message),
            c_length,
        )
    }
    // Register the callback.
    tox.onFriendStatusMessage(tox, friendNumber, message)
}

//export callback_friend_status
func callback_friend_status(
    c_tox          *C.Tox,
    c_friend_number C.uint32_t,
    c_status        C.TOX_USER_STATUS,
    c_user_data     unsafe.Pointer,
) {
    // Initialize variables.
    tox := (*Tox)(c_user_data)
    friendNumber := uint32(c_friend_number)
    var status ToxUserStatus
    switch c_status {
        case C.TOX_USER_STATUS_NONE:
            status = ToxUserStatusNone
        case C.TOX_USER_STATUS_AWAY:
            status = ToxUserStatusAway
        case C.TOX_USER_STATUS_BUSY:
            status = ToxUserStatusBusy
    }
    // Register the callback.
    tox.onFriendStatus(tox, friendNumber, status)
}

//export callback_friend_connection_status
func callback_friend_connection_status(
    c_tox              *C.Tox,
    c_friend_number     C.uint32_t,
    c_connection_status C.TOX_CONNECTION,
    c_user_data         unsafe.Pointer,
) {
    // Initialize variables.
    tox := (*Tox)(c_user_data)
    friendNumber := uint32(c_friend_number)
    var connectionStatus ToxConnectionStatus
    switch c_connection_status {
        case C.TOX_CONNECTION_NONE:
            connectionStatus = ToxConnectionNone
        case C.TOX_CONNECTION_TCP:
            connectionStatus = ToxConnectionTCP
        case C.TOX_CONNECTION_UDP:
            connectionStatus = ToxConnectionUDP
    }
    // Register the callback.
    tox.onFriendConnectionStatus(tox, friendNumber, connectionStatus)
}

//export callback_friend_message
func callback_friend_message(
    c_tox          *C.Tox,
    c_friend_number C.uint32_t,
    c_message_type  C.TOX_MESSAGE_TYPE,
    c_message      *C.uint8_t,
    c_length        C.size_t,
    c_user_data     unsafe.Pointer,
) {
    // Initialize variables.
    tox := (*Tox)(c_user_data)
    friendNumber := uint32(c_friend_number)
    var messageType ToxMessageType
    switch c_message_type {
        case C.TOX_MESSAGE_TYPE_NORMAL:
            messageType = ToxMessageTypeNormal
        case C.TOX_MESSAGE_TYPE_ACTION:
            messageType = ToxMessageTypeAction
    }
    message := make([]byte, c_length)
    // Copy the chat message.
    if (c_length > 0) {
        C.memcpy(
            unsafe.Pointer(&message[0]),
            unsafe.Pointer(c_message),
            c_length,
        )
    }
    // Register the callback.
    tox.onFriendMessage(tox, friendNumber, messageType, message)
}

//export callback_friend_lossless_packet
func callback_friend_lossless_packet(
    c_tox          *C.Tox,
    c_friend_number C.uint32_t,
    c_data         *C.uint8_t,
    c_length        C.size_t,
    c_user_data     unsafe.Pointer,
) {
    // Initialize variables.
    tox := (*Tox)(c_user_data)
    friendNumber := uint32(c_friend_number)
    data := make([]byte, c_length)
    // Copy the data.
    if (c_length > 0) {
        C.memcpy(
            unsafe.Pointer(&data[0]),
            unsafe.Pointer(c_data),
            c_length,
        )
    }
    // Register the callback.
    tox.onFriendLosslessPacket(tox, friendNumber, data)
}
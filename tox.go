/* Module      : tox.go
 * Description : High-level bindings to Tox core.
 * Copyright   : Copyright (c) 2015 Vaurum Labs Inc. All rights reserved.
 * License     : GPLv3
 * Maintainer  : Dominic Williams <dominic@mirrorx.com>, Enzo Haussecker <enzo@mirrorx.com>
 * Stability   : Experimental
 * Portability : Non-portable (requires Tox core at commit fc54980)
 *
 * This module establishes the high-level API that allows clients to communicate
 * using the Tox protocol.
 */

package libtox

import (
    "unsafe"
    "encoding/hex"
    "time"
)

////////////////////////////////////////////////////////////////////////////////
////////////////////////// FOREIGN FUNCTION INTERFACE //////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*

#cgo LDFLAGS: -l toxcore

#include <tox/tox.h>
#include <stdlib.h>

void callback_self_connection_status(struct Tox *, TOX_CONNECTION, void *);
void callback_friend_name(struct Tox *, uint32_t, const uint8_t *, size_t, void *);
void callback_friend_request(struct Tox *, const uint8_t *, const uint8_t *, size_t, void *);
void callback_friend_status_message(struct Tox *, uint32_t, const uint8_t *, size_t, void *);
void callback_friend_status(struct Tox *, uint32_t, TOX_USER_STATUS, void *);
void callback_friend_connection_status(struct Tox *, uint32_t, TOX_CONNECTION, void *);
void callback_friend_message(struct Tox *, uint32_t, TOX_MESSAGE_TYPE, const uint8_t *, size_t, void *);
void callback_friend_lossless_packet(struct Tox *, uint32_t, const uint8_t *, size_t, void *);

// We cannot register our callbacks directly from Go. This macro creates a C
// function that registers a pointer to our callback function defined in Go.
#define GEN_CALLBACK_API(x) \
static void register_##x(Tox *tox, void *t) { \
    tox_callback_##x(tox, callback_##x, t); \
}

GEN_CALLBACK_API(self_connection_status)
GEN_CALLBACK_API(friend_name)
GEN_CALLBACK_API(friend_request)
GEN_CALLBACK_API(friend_status_message)
GEN_CALLBACK_API(friend_status)
GEN_CALLBACK_API(friend_connection_status)
GEN_CALLBACK_API(friend_message)
GEN_CALLBACK_API(friend_lossless_packet)

 */
import "C"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////// INSTANCE LIFECYCLE //////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Create or restore a Tox instance. This will bring the instance into a valid
// state. If the startup options parameter is null, then the default options
// are used. If the data parameter is not null, then this function will attempt
// to restore the instance from that state. The user data parameter can be used
// to pass custom information to callback functions.
func New(options *ToxOptions, data []byte, userData unsafe.Pointer) (tox *Tox, throw error) {
    // Create some uninitialized variables for interfacing with C.
    var c_options *C.struct_Tox_Options
    var c_data    *C.uint8_t
    var c_length   C.size_t
    var c_tox     *C.Tox
    var c_error    C.TOX_ERR_NEW
    // Check if any custom startup options were provided.
    if (options != nil) {
        // Convert the startup options to their C-side equivilant.
        c_options = options.cOptions()
        defer C.free(unsafe.Pointer(c_options.proxy_host))
    }
    // Check if any previously saved data was provided.
    if (data != nil) {
        // Assign the pointer to the location of that data. 
        c_data = (*C.uint8_t)(&data[0])
    }
    // Get the length of the previously saved data.
    c_length = C.size_t(len(data))
    // Create a Tox instance.
    c_tox = C.tox_new(c_options, c_data, c_length, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_NEW_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_NEW_NULL:
                throw = ToxErrNewNull
            case C.TOX_ERR_NEW_MALLOC:
                throw = ToxErrNewMalloc
            case C.TOX_ERR_NEW_PORT_ALLOC:
                throw = ToxErrNewPortAlloc
            case C.TOX_ERR_NEW_PROXY_BAD_TYPE:
                throw = ToxErrNewProxyBadType
            case C.TOX_ERR_NEW_PROXY_BAD_HOST:
                throw = ToxErrNewProxyBadHost
            case C.TOX_ERR_NEW_PROXY_BAD_PORT:
                throw = ToxErrNewProxyBadPort
            case C.TOX_ERR_NEW_PROXY_NOT_FOUND:
                throw = ToxErrNewProxyNotFound
            case C.TOX_ERR_NEW_LOAD_ENCRYPTED:
                throw = ToxErrNewLoadEncrypted
            case C.TOX_ERR_NEW_LOAD_BAD_FORMAT:
                throw = ToxErrNewLoadBadFormat
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Assign the Tox instance to the return value.
        tox = &Tox{ handle:c_tox, UserData: userData }
    }
    return
}

// Write all information associated with a Tox instance to a byte array.
func (tox *Tox) Serialize() (data []byte) {
    // Get the length of the Tox instance data.
    c_length := C.tox_get_savedata_size(tox.handle)
    // Initialize the return value with a byte array of that length.
    data = make([]byte, c_length)
    // Check if the byte array can be indexed at zero.
    if (c_length > 0) {
        // Copy the data to the return value.
        C.tox_get_savedata(tox.handle, (*C.uint8_t)(&data[0]))
    }
    return
}

// Destroy a Tox instance. This will disconnect the instance from the Tox
// network and release all other resources associated with it. The Tox pointer
// becomes invalid and can no longer be used.
func (tox *Tox) Destroy() {
    C.tox_kill(tox.handle)
}

////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// STARTUP OPTIONS ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Get the default Tox client startup options.
func DefaultOptions() (options *ToxOptions) {
    // Create a Tox_Options struct.
    var c_options C.struct_Tox_Options
    // Populate the struct with the default startup options.
    C.tox_options_default(&c_options)
    // Initialize the return object.
    options = &ToxOptions{}
    // Assign the default startup options to the return object.
    options.assign(&c_options)
    return
}

// Create a C struct to hold the Tox client startup options. This struct
// contains C heap items that must be freed to prevent memory leaks.
func (options *ToxOptions) cOptions() (c_options *C.struct_Tox_Options) {
    // Initialize the return object.
    c_options = &C.struct_Tox_Options{}
    // Copy the fields.
    c_options.ipv6_enabled = C.bool(options.IPv6Enabled)
    c_options.udp_enabled  = C.bool(options.UDPEnabled)
    c_options.proxy_type   = C.TOX_PROXY_TYPE(options.ProxyType)
    c_options.proxy_host   = C.CString(options.ProxyHost)
    c_options.proxy_port   = C.uint16_t(options.ProxyPort)
    c_options.start_port   = C.uint16_t(options.StartPort)
    c_options.end_port     = C.uint16_t(options.EndPort)
    return
}

// Assign the Tox client startup options to the Go struct.
func (options *ToxOptions) assign(c_options *C.struct_Tox_Options) {
    // Copy the fields.
    options.IPv6Enabled = bool(c_options.ipv6_enabled)
    options.UDPEnabled  = bool(c_options.udp_enabled)
    options.ProxyType   = ToxProxyType(c_options.proxy_type)
    options.ProxyHost   = C.GoString(c_options.proxy_host)
    options.ProxyPort   = uint16(c_options.proxy_port)
    options.StartPort   = uint16(c_options.start_port)
    options.EndPort     = uint16(c_options.end_port)
}

////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// EVENT PROCESSING ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Run the main event processing loop.
func (tox *Tox) Process() {
    tox.mutex.Lock()
    defer tox.mutex.Unlock()
    // Iterate inside the lock.
    C.tox_iterate(tox.handle)
}

// Get the iteration interval in milliseconds.
func (tox *Tox) ProcessingDelay() time.Duration {
    c_millis := C.tox_iteration_interval(tox.handle)
    return time.Duration(uint32(c_millis)) * time.Millisecond
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////// CALLBACK FUNCTIONS //////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// This function registers a function that executes when the connection status
// of the client changes.
func (tox *Tox) SetOnConnectionStatus(callback OnConnectionStatus) {
    tox.onConnectionStatus = callback
    C.register_self_connection_status(tox.handle, unsafe.Pointer(tox))
}

// This function registers a function that executes when receiving a friend
// request.
func (tox *Tox) SetOnFriendRequest(callback OnFriendRequest) {
    tox.onFriendRequest = callback
    C.register_friend_request(tox.handle, unsafe.Pointer(tox))
}

// This function registers a function that executes when a friend changes their
// name.
func (tox *Tox) SetOnFriendName(callback OnFriendName) {
    tox.onFriendName = callback
    C.register_friend_name(tox.handle, unsafe.Pointer(tox))
}

// This function registers a function that executes when a friend changes their
// status.
func (tox *Tox) SetOnFriendStatus(callback OnFriendStatus) {
    tox.onFriendStatus = callback
    C.register_friend_status(tox.handle, unsafe.Pointer(tox))
}

// This function registers a function that executes when a friend changes their
// status message.
func (tox *Tox) SetOnFriendStatusMessage(callback OnFriendStatusMessage) {
    tox.onFriendStatusMessage = callback
    C.register_friend_status_message(tox.handle, unsafe.Pointer(tox))
}

// This function registers a function that executes when the connection status
// of a friend changes.
func (tox *Tox) SetOnFriendConnectionStatus(callback OnFriendConnectionStatus) {
    tox.onFriendConnectionStatus = callback
    C.register_friend_connection_status(tox.handle, unsafe.Pointer(tox))
}

// This function registers a function that executes when receiving a chat
// message from a friend.
func (tox *Tox) SetOnFriendMessage(callback OnFriendMessage) {
    tox.onFriendMessage = callback
    C.register_friend_message(tox.handle, unsafe.Pointer(tox))
}

// This function registers a function that executes when receiving a custom
// loss-less packet from a friend.
func (tox *Tox) SetOnFriendLosslessPacket(callback OnFriendLosslessPacket) {
    tox.onFriendLosslessPacket = callback
    C.register_friend_lossless_packet(tox.handle, unsafe.Pointer(tox))
}

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// CLIENT STATE /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Get the address of the Tox client.
func (tox *Tox) GetAddress() (address ToxAddress) {
    // Create a pointer to the return value.
    c_address := (*C.uint8_t)(&address[0])
    // Copy the address to the return value.
    C.tox_self_get_address(tox.handle, c_address)
    return
}

// Get the no-spam value of the Tox client.
func (tox *Tox) GetNoSpam() uint32 {
    return uint32(C.tox_self_get_nospam(tox.handle))
}

// Set the no-spam value of the Tox client.
func (tox *Tox) SetNoSpam(nospam uint32) {
    C.tox_self_set_nospam(tox.handle, C.uint32_t(nospam))
}

// Get the public key of the Tox client.
func (tox *Tox) GetPublicKey() (publicKey ToxPublicKey) {
    // Create a pointer to the return value.
    c_public_key := (*C.uint8_t)(&publicKey[0])
    // Copy the public key to the return value.
    C.tox_self_get_public_key(tox.handle, c_public_key)
    return
}

// Get the secret key of the Tox client.
func (tox *Tox) GetSecretKey() (secretKey ToxSecretKey) {
    // Create a pointer to the return value.
    c_secret_key := (*C.uint8_t)(&secretKey[0])
    // Copy the secret key to the return value.
    C.tox_self_get_secret_key(tox.handle, c_secret_key)
    return
}

// Get the name of the Tox client.
func (tox *Tox) GetName() (name []byte) {
    // Get the length of the name.
    c_length := C.tox_self_get_name_size(tox.handle)
    // Initialize the return value with a byte array of that length.
    name = make([]byte, c_length)
    // Create a pointer to pass into tox_self_get_name.
    var c_name *C.uint8_t
    // Check if the return value can be indexed at zero.
    if (c_length > 0) {
        // Assign the pointer to the location of the return value.
        c_name = (*C.uint8_t)(&name[0])
    }
    // Copy the name to the return value.
    C.tox_self_get_name(tox.handle, c_name)
    return
}

// Set the name of the Tox client.
func (tox *Tox) SetName(name []byte) (throw error) {
    // Get the length of the name.
    c_length := C.size_t(len(name))
    // Create additional variables to pass into tox_self_set_name.
    var c_name *C.uint8_t
    var c_error C.TOX_ERR_SET_INFO
    // Check if the name contains data.
    if (c_length > 0) {
        // Assign the pointer to the location of that data.
        c_name = (*C.uint8_t)(&name[0])
    }
    // Copy the name to the client state.
    C.tox_self_set_name(tox.handle, c_name, c_length, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_SET_INFO_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_SET_INFO_NULL:
                throw = ToxErrSetInfoNull
            case C.TOX_ERR_SET_INFO_TOO_LONG:
                throw = ToxErrSetInfoTooLong
            default:
                throw = ToxErrUnknown
        }
    }
    return
}

// Get the status of the Tox client.
func (tox *Tox) GetStatus() (status ToxUserStatus) {
    // Get the status as a C type.
    c_status := C.tox_self_get_status(tox.handle)
    // Convert that to a Go type.
    switch c_status {
        case C.TOX_USER_STATUS_NONE:
            status = ToxUserStatusNone
        case C.TOX_USER_STATUS_AWAY:
            status = ToxUserStatusAway
        case C.TOX_USER_STATUS_BUSY:
            status = ToxUserStatusBusy
    }
    return
}

// Set the status of the Tox client.
func (tox *Tox) SetStatus(status ToxUserStatus) {
    // Create a TOX_USER_STATUS variable.
    var c_status C.TOX_USER_STATUS
    // Assign that variable according to the input value.
    switch status {
        case ToxUserStatusNone:
            c_status = C.TOX_USER_STATUS_NONE
        case ToxUserStatusAway:
            c_status = C.TOX_USER_STATUS_AWAY
        case ToxUserStatusBusy:
            c_status = C.TOX_USER_STATUS_BUSY
    }
    // Copy that variable to the Tox client status.
    C.tox_self_set_status(tox.handle, c_status)
}

// Get the status message of the Tox client.
func (tox *Tox) GetStatusMessage() (message []byte) {
    // Get the length of the status message.
    c_length := C.tox_self_get_status_message_size(tox.handle)
    // Initialize the return value with a byte array of that length.
    message = make([]byte, c_length)
    // Create a pointer to pass into tox_self_get_status_message.
    var c_message *C.uint8_t
    // Check if the status message contains data.
    if (c_length > 0) {
        // Assign the pointer to the location of the return value.
        c_message = (*C.uint8_t)(&message[0])
    }
    // Copy the status message to the return value.
    C.tox_self_get_status_message(tox.handle, c_message)
    return
}

// Set the status message of the Tox client.
func (tox *Tox) SetStatusMessage(message []byte) (throw error) {
    // Get the length of the status message.
    c_length := C.size_t(len(message))
    // Create additional variables to pass into tox_self_set_status_message.
    var c_message *C.uint8_t
    var c_error    C.TOX_ERR_SET_INFO
    // Check if the status message contains data.
    if (c_length > 0) {
        // Assign the pointer to the location of the status message.
        c_message = (*C.uint8_t)(&message[0])
    }
    // Copy the status message to the client state.
    C.tox_self_set_status_message(tox.handle, c_message, c_length, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_SET_INFO_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_SET_INFO_NULL:
                throw = ToxErrSetInfoNull
            case C.TOX_ERR_SET_INFO_TOO_LONG:
                throw = ToxErrSetInfoTooLong
            default:
                throw = ToxErrUnknown
        }
    }
    return
}

// Get the connection status of the Tox client.
func (tox *Tox) GetConnectionStatus() (connectionStatus ToxConnectionStatus) {
    // Get the connection status as a C type.
    c_connection_status := C.tox_self_get_connection_status(tox.handle)
    // Convert that to a Go type.
    switch c_connection_status {
        case C.TOX_CONNECTION_NONE:
            connectionStatus = ToxConnectionNone
        case C.TOX_CONNECTION_TCP:
            connectionStatus = ToxConnectionTCP
        case C.TOX_CONNECTION_UDP:
            connectionStatus = ToxConnectionUDP
    }
    return
}

// Get the friend list of the Tox client.
func (tox *Tox) GetFriendList() (friendList []uint32) {
    // Get the length of the friend list.
    c_length := C.tox_self_get_friend_list_size(tox.handle)
    // Initialize the return value with an integer array of that length.
    friendList = make([]uint32, c_length)
    // Create a pointer to pass into tox_self_get_friend_list.
    var c_friend_list *C.uint32_t
    // Check if the friend list contains data.
    if (c_length > 0) {
        // Assign the pointer to the location of the return value.
        c_friend_list = (*C.uint32_t)(&friendList[0])
    }
    // Copy the friend list to the return value.
    C.tox_self_get_friend_list(tox.handle, c_friend_list)
    return
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////// FRIEND MANAGEMENT ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Add a friend.
func (tox *Tox) FriendAdd(address ToxAddress, message []byte) (friendNumber uint32, throw error) {
    // Create a pointer to the address of the friend.
    c_address := (*C.uint8_t)(&address[0])
    // Get the length of the friend request message.
    c_length := C.size_t(len(message))
    // Create additional variables to pass into tox_friend_add.
    var c_message      *C.uint8_t
    var c_error         C.TOX_ERR_FRIEND_ADD
    var c_friend_number C.uint32_t
    // Check if the friend request message contains data.
    if (c_length > 0) {
        // Assign the friend request message
        // pointer to the location of that data.
        c_message = (*C.uint8_t)(&message[0])
    }
    // Add the friend.
    c_friend_number = C.tox_friend_add(tox.handle, c_address, c_message, c_length, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_ADD_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_ADD_NULL:
                throw = ToxErrFriendAddNull
            case C.TOX_ERR_FRIEND_ADD_TOO_LONG:
                throw = ToxErrFriendAddTooLong
            case C.TOX_ERR_FRIEND_ADD_NO_MESSAGE:
                throw = ToxErrFriendAddNoMessage
            case C.TOX_ERR_FRIEND_ADD_OWN_KEY:
                throw = ToxErrFriendAddOwnKey
            case C.TOX_ERR_FRIEND_ADD_ALREADY_SENT:
                throw = ToxErrFriendAddAlreadySent
            case C.TOX_ERR_FRIEND_ADD_BAD_CHECKSUM:
                throw = ToxErrFriendAddBadChecksum
            case C.TOX_ERR_FRIEND_ADD_SET_NEW_NOSPAM:
                throw = ToxErrFriendAddSetNewNoSpam
            case C.TOX_ERR_FRIEND_ADD_MALLOC:
                throw = ToxErrFriendAddMalloc
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Assign the friend number to the return value.
        friendNumber = uint32(c_friend_number)
    }
    return
}

// Add a friend without sending a friend request.
func (tox *Tox) FriendAddNoRequest(publicKey ToxPublicKey) (friendNumber uint32, throw error) {
    // Create a pointer to the public key.
    c_public_key := (*C.uint8_t)(&publicKey[0])
    // Create additional variables to pass into tox_friend_add_norequest.
    var c_error         C.TOX_ERR_FRIEND_ADD
    var c_friend_number C.uint32_t
    // Add the friend.
    c_friend_number = C.tox_friend_add_norequest(tox.handle, c_public_key, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_ADD_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_ADD_NULL:
                throw = ToxErrFriendAddNull
            case C.TOX_ERR_FRIEND_ADD_TOO_LONG:
                throw = ToxErrFriendAddTooLong
            case C.TOX_ERR_FRIEND_ADD_NO_MESSAGE:
                throw = ToxErrFriendAddNoMessage
            case C.TOX_ERR_FRIEND_ADD_OWN_KEY:
                throw = ToxErrFriendAddOwnKey
            case C.TOX_ERR_FRIEND_ADD_ALREADY_SENT:
                throw = ToxErrFriendAddAlreadySent
            case C.TOX_ERR_FRIEND_ADD_BAD_CHECKSUM:
                throw = ToxErrFriendAddBadChecksum
            case C.TOX_ERR_FRIEND_ADD_SET_NEW_NOSPAM:
                throw = ToxErrFriendAddSetNewNoSpam
            case C.TOX_ERR_FRIEND_ADD_MALLOC:
                throw = ToxErrFriendAddMalloc
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Assign the friend number to the return value.
        friendNumber = uint32(c_friend_number)
    }
    return
}

// Delete a friend.
func (tox *Tox) FriendDelete(friendNumber uint32) (throw error) {
    // Create a variable for the friend number.
    c_friend_number := C.uint32_t(friendNumber)
    // Create an uninitialized error variable.
    var c_error C.TOX_ERR_FRIEND_DELETE
    // Delete the friend.
    C.tox_friend_delete(tox.handle, c_friend_number, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_DELETE_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_DELETE_FRIEND_NOT_FOUND:
                throw = ToxErrFriendDeleteFriendNotFound
            default:
                throw = ToxErrUnknown
        }
    }
    return
}

// Check if a friend exists.
func (tox *Tox) FriendExists(friendNumber uint32) bool {
    // Create a variable for the friend number.
    c_friend_number := C.uint32_t(friendNumber)
    // Return the boolean result.
    return bool(C.tox_friend_exists(tox.handle, c_friend_number))
}

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// FRIEND STATE /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Get the name of a friend.
func (tox *Tox) FriendGetName(friendNumber uint32) (name []byte, throw error) {
    // Create a variable for the friend number.
    c_friend_number := C.uint32_t(friendNumber)
    // Create other variables to pass into the C functions.
    var c_error  C.TOX_ERR_FRIEND_QUERY
    var c_name  *C.uint8_t
    var c_length C.size_t
    // Get the length of the friend name.
    c_length = C.tox_friend_get_name_size(tox.handle, c_friend_number, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_QUERY_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_QUERY_NULL:
                throw = ToxErrFriendQueryNull
            case C.TOX_ERR_FRIEND_QUERY_FRIEND_NOT_FOUND:
                throw = ToxErrFriendQueryFriendNotFound
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Initialize the name.
        name = make([]byte, c_length)
        // Check if the name contains data.
        if (c_length > 0) {
            // Assign the pointer to the location of that data.
            c_name = (*C.uint8_t)(&name[0])
        }
        // Get the name of the friend.
        C.tox_friend_get_name(tox.handle, c_friend_number, c_name, &c_error)
        // Check if an error occurred.
        if (c_error != C.TOX_ERR_FRIEND_QUERY_OK) {
            // Nullify the name.
            name = nil
            // Assign the error to the return value.
            switch c_error {
                case C.TOX_ERR_FRIEND_QUERY_NULL:
                    throw = ToxErrFriendQueryNull
                case C.TOX_ERR_FRIEND_QUERY_FRIEND_NOT_FOUND:
                    throw = ToxErrFriendQueryFriendNotFound
                default:
                    throw = ToxErrUnknown
            }
        }
    }
    return
}

// Get the public key of a friend.
func (tox *Tox) FriendGetPublicKey(friendNumber uint32) (publicKey ToxPublicKey, throw error) {
    // Create variables for friend number and public key.
    c_friend_number := C.uint32_t(friendNumber)
    c_public_key    := (*C.uint8_t)(&publicKey[0])
    // Create an uninitialized error variable.
    var c_error C.TOX_ERR_FRIEND_GET_PUBLIC_KEY
    // Copy the public key of the friend to the return value.
    C.tox_friend_get_public_key(tox.handle, c_friend_number, c_public_key, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_GET_PUBLIC_KEY_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_GET_PUBLIC_KEY_FRIEND_NOT_FOUND:
                throw = ToxErrFriendGetPublicKeyFriendNotFound
            default:
                throw = ToxErrUnknown
        }
    }
    return
}

// Get the friend associated with the given public key.
func (tox *Tox) FriendByPublicKey(publicKey ToxPublicKey) (friendNumber uint32, throw error) {
    // Create a pointer to the public key of the friend.
    c_public_key := (*C.uint8_t)(&publicKey[0])
    // Create uninitialized variables for error and friend number.
    var c_error         C.TOX_ERR_FRIEND_BY_PUBLIC_KEY
    var c_friend_number C.uint32_t
    // Get the friend number associated with the public key.
    c_friend_number = C.tox_friend_by_public_key(tox.handle, c_public_key, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_BY_PUBLIC_KEY_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_BY_PUBLIC_KEY_NULL:
                throw = ToxErrFriendByPublicKeyNull
            case C.TOX_ERR_FRIEND_BY_PUBLIC_KEY_NOT_FOUND:
                throw = ToxErrFriendByPublicKeyNotFound
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Assign the friend number to the return value.
        friendNumber = uint32(c_friend_number)
    }
    return
}

// Get the status of a friend.
func (tox *Tox) FriendGetStatus(friendNumber uint32) (status ToxUserStatus, throw error) {
    // Create a variable for the friend number.
    c_friend_number := C.uint32_t(friendNumber)
    // Create uninitialized variables for error and status.
    var c_error  C.TOX_ERR_FRIEND_QUERY
    var c_status C.TOX_USER_STATUS
    // Get the status.
    c_status = C.tox_friend_get_status(tox.handle, c_friend_number, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_QUERY_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_QUERY_NULL:
                throw = ToxErrFriendQueryNull
            case C.TOX_ERR_FRIEND_QUERY_FRIEND_NOT_FOUND:
                throw = ToxErrFriendQueryFriendNotFound
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Assign the status to the return value.
        switch c_status {
            case C.TOX_USER_STATUS_NONE:
                status = ToxUserStatusNone
            case C.TOX_USER_STATUS_AWAY:
                status = ToxUserStatusAway
            case C.TOX_USER_STATUS_BUSY:
                status = ToxUserStatusBusy
        }
    }
    return
}

// Get the status message of a friend.
func (tox *Tox) FriendGetStatusMessage(friendNumber uint32) (message []byte, throw error) {
    // Create a variable for the friend number.
    c_friend_number := C.uint32_t(friendNumber)
    // Create uninitialized variables for error,
    // status message and status message length.
    var c_error    C.TOX_ERR_FRIEND_QUERY
    var c_message *C.uint8_t
    var c_length   C.size_t
    // Get the status message length.
    c_length = C.tox_friend_get_status_message_size(tox.handle, c_friend_number, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_QUERY_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_QUERY_NULL:
                throw = ToxErrFriendQueryNull
            case C.TOX_ERR_FRIEND_QUERY_FRIEND_NOT_FOUND:
                throw = ToxErrFriendQueryFriendNotFound
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Initialize the status message.
        message = make([]byte, c_length)
        // Check if the status message contains data.
        if (c_length > 0) {
            // Assign the status message pointer to the location of that data.
            c_message = (*C.uint8_t)(&message[0])
        }
        // Get the status message and copy it to the return value.
        C.tox_friend_get_status_message(tox.handle, c_friend_number, c_message, &c_error)
        // Check if an error occurred.
        if (c_error != C.TOX_ERR_FRIEND_QUERY_OK) {
            // Nullify the status message.
            message = nil
            // Assign the error to the return value.
            switch c_error {
                case C.TOX_ERR_FRIEND_QUERY_NULL:
                    throw = ToxErrFriendQueryNull
                case C.TOX_ERR_FRIEND_QUERY_FRIEND_NOT_FOUND:
                    throw = ToxErrFriendQueryFriendNotFound
                default:
                    throw = ToxErrUnknown
            }
        }
    }
    return
}

// Get the connection status of a friend.
func (tox *Tox) FriendGetConnectionStatus(friendNumber uint32) (connectionStatus ToxConnectionStatus, throw error) {
    // Create a variable for the friend number.
    c_friend_number := C.uint32_t(friendNumber)
    // Create additional variables for error and connection status.
    var c_error             C.TOX_ERR_FRIEND_QUERY
    var c_connection_status C.TOX_CONNECTION
    // Get the connection status.
    c_connection_status = C.tox_friend_get_connection_status(tox.handle, c_friend_number, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_QUERY_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_QUERY_NULL:
                throw = ToxErrFriendQueryNull
            case C.TOX_ERR_FRIEND_QUERY_FRIEND_NOT_FOUND:
                throw = ToxErrFriendQueryFriendNotFound
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Assign the connection status to the return value.
        switch c_connection_status {
            case C.TOX_CONNECTION_NONE:
                connectionStatus = ToxConnectionNone
            case C.TOX_CONNECTION_TCP:
                connectionStatus = ToxConnectionTCP
            case C.TOX_CONNECTION_UDP:
                connectionStatus = ToxConnectionUDP
        }
    }
    return
}

// Get the last time a friend was seen online.
func (tox *Tox) FriendGetLastOnline(friendNumber uint32) (timestamp time.Time, throw error) {
    // Create a variable for the friend number.
    c_friend_number := C.uint32_t(friendNumber)
    // Create additional variables for error and timestamp.
    var c_error     C.TOX_ERR_FRIEND_GET_LAST_ONLINE
    var c_timestamp C.uint64_t
    // Get the timestamp.
    c_timestamp = C.tox_friend_get_last_online(tox.handle, c_friend_number, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_GET_LAST_ONLINE_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_GET_LAST_ONLINE_FRIEND_NOT_FOUND:
                throw = ToxErrFriendGetLastOnlineFriendNotFound
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Assign the timestamp to the return value.
        timestamp = time.Unix(int64(c_timestamp), 0)
    }
    return
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////// DATA TRANSMISSION ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Send a chat message to an online friend.
func (tox *Tox) FriendSendMessage(friendNumber uint32, messageType ToxMessageType, message []byte) (messageId uint32, throw error) {
    // Create and initialize variables.
    c_friend_number := C.uint32_t(friendNumber)
    c_length        := C.size_t(len(message))
    var c_message_type C.TOX_MESSAGE_TYPE
    var c_message     *C.uint8_t
    var c_error        C.TOX_ERR_FRIEND_SEND_MESSAGE
    var c_message_id   C.uint32_t
    // Initialize the message type according to the input value.
    switch messageType {
        case ToxMessageTypeNormal:
            c_message_type = C.TOX_MESSAGE_TYPE_NORMAL
        case ToxMessageTypeAction:
            c_message_type = C.TOX_MESSAGE_TYPE_ACTION
    }
    // Check if the chat message contains data.
    if (c_length > 0) {
        // Assign the chat message pointer to the location of that data.
        c_message = (*C.uint8_t)(&message[0])
    }
    // Send the chat message.
    c_message_id = C.tox_friend_send_message(tox.handle, c_friend_number, c_message_type, c_message, c_length, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_SEND_MESSAGE_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_SEND_MESSAGE_NULL:
                throw = ToxErrFriendSendMessageNull
            case C.TOX_ERR_FRIEND_SEND_MESSAGE_FRIEND_NOT_FOUND:
                throw = ToxErrFriendSendMessageFriendNotFound
            case C.TOX_ERR_FRIEND_SEND_MESSAGE_FRIEND_NOT_CONNECTED:
                throw = ToxErrFriendSendMessageFriendNotConnected
            case C.TOX_ERR_FRIEND_SEND_MESSAGE_SENDQ:
                throw = ToxErrFriendSendMessageSendQ
            case C.TOX_ERR_FRIEND_SEND_MESSAGE_TOO_LONG:
                throw = ToxErrFriendSendMessageTooLong
            case C.TOX_ERR_FRIEND_SEND_MESSAGE_EMPTY:
                throw = ToxErrFriendSendMessageEmpty
            default:
                throw = ToxErrUnknown
        }
    } else {
        // Assign the chat message ID to the return value.
        messageId = uint32(c_message_id)
    }
    return
}

// Send a custom loss-less packet to an online friend.
func (tox *Tox) FriendSendLosslessPacket(friendNumber uint32, data []byte) (throw error) {
    // Create and initialize variables.
    c_friend_number := C.uint32_t(friendNumber)
    c_length        := C.size_t(len(data))
    var c_data *C.uint8_t
    var c_error C.TOX_ERR_FRIEND_CUSTOM_PACKET
    // Check if data was provided.
    if (c_length > 0) {
        // Assign the pointer to the location of that data.
        c_data = (*C.uint8_t)(&data[0])
    }
    // Send the packet.
    C.tox_friend_send_lossless_packet(tox.handle, c_friend_number, c_data, c_length, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_FRIEND_CUSTOM_PACKET_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_FRIEND_CUSTOM_PACKET_NULL:
                throw = ToxErrFriendCustomPacketNull
            case C.TOX_ERR_FRIEND_CUSTOM_PACKET_FRIEND_NOT_FOUND:
                throw = ToxErrFriendCustomPacketFriendNotFound
            case C.TOX_ERR_FRIEND_CUSTOM_PACKET_FRIEND_NOT_CONNECTED:
                throw = ToxErrFriendCustomPacketFriendNotConnected
            case C.TOX_ERR_FRIEND_CUSTOM_PACKET_INVALID:
                throw = ToxErrFriendCustomPacketInvalid
            case C.TOX_ERR_FRIEND_CUSTOM_PACKET_EMPTY:
                throw = ToxErrFriendCustomPacketEmpty
            case C.TOX_ERR_FRIEND_CUSTOM_PACKET_TOO_LONG:
                throw = ToxErrFriendCustomPacketTooLong
            case C.TOX_ERR_FRIEND_CUSTOM_PACKET_SENDQ:
                throw = ToxErrFriendCustomPacketSendQ
            default:
                throw = ToxErrUnknown
        }
    }
    return
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// NETWORKING //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// This is the default seed node for this library. It is used for testing
// purposes only. Never use this in production since the IP address is subject
// to change.
func DefaultSeedNode() *SeedNode {
    return NewSeedNode(
        "192.254.75.102",
        33445,
        "951C88B7E75C867418ACDB5D273821372BB5BD652740BCDF623A4FA293E75D2F",
    )
}

// This function creates a new seed node. You can find an active list of nodes
// at https://wiki.tox.im/Nodes
func NewSeedNode(host string, port uint16, publicKey string) *SeedNode {
    return &SeedNode { Host: host, Port: port, PublicKey: publicKey }
}

// This function sends a "get nodes" request to the given seed node with IP,
// port, and public key to setup connections. It will attempt to connect to the
// node using UDP and TCP at the same time. Tox will use the node as a TCP
// relay in case ToxOptions.UDPEnabled was false, and also to connect to
// friends that are in TCP-only mode. Tox will also use the TCP connection when
// NAT hole punching is slow, and later switch to UDP if hole punching succeeds.
func (tox *Tox) Bootstrap(seedNode *SeedNode) (throw error) {
    // Create and initialize variables.
    c_host := C.CString(seedNode.Host)
    defer C.free(unsafe.Pointer(c_host))
    c_port := C.uint16_t(seedNode.Port)
    pubkey, err := hex.DecodeString(seedNode.PublicKey)
    if err != nil {
        throw = err
    }
    c_public_key := (*C.uint8_t)(&pubkey[0])
    var c_error C.TOX_ERR_BOOTSTRAP
    // Bootstrap the network connection.
    C.tox_bootstrap(tox.handle, c_host, c_port, c_public_key, &c_error)
    // Check if an error occurred.
    if (c_error != C.TOX_ERR_BOOTSTRAP_OK) {
        // Assign the error to the return value.
        switch c_error {
            case C.TOX_ERR_BOOTSTRAP_NULL:
                throw = ToxErrBootstrapNull
            case C.TOX_ERR_BOOTSTRAP_BAD_HOST:
                throw = ToxErrBootstrapBadHost
            case C.TOX_ERR_BOOTSTRAP_BAD_PORT:
                throw = ToxErrBootstrapBadPort
            default:
                throw = ToxErrUnknown
        }
    }
    return
}
libtox 1.0
==========

[![Build Status](https://api.travis-ci.org/mirrorx/libtox.svg?branch=master)](https://travis-ci.org/mirrorx/libtox)

### Installation

You should get a clone of the `libtox` repository, and then build the source code from inside the source directory, but first, you will need to install `toxcore` at commit fc54980. The `toxcore` has had a rather unstable API, so it is important to get the commit ID correct. Past and future versions may not be compatible with these bindings. You will also need the `curve25519` package.

```bash
git clone https://github.com/irungentoo/toxcore
pushd toxcore
git checkout fc549805c1abfeac3d73f88aa95c1f58dc5ef499
autoreconf --install
sh configure
make
sudo make install
popd
git clone https://github.com/mirrorx/libtox
pushd libtox
go get golang.org/x/crypto/curve25519
go build
popd
```

### Resources

Feel free to contact us if you have any questions or comments regarding this package.
- [Enzo Haussecker](mailto:enzo@mirrorx.com)
- [Dominic Williams](mailto:dominic@mirrorx.com)

/* Module      : tox_test.go
 * Description : Test suite for high-level bindings to Tox core.
 * Copyright   : Copyright (c) 2015 Vaurum Labs Inc. All rights reserved.
 * License     : GPLv3
 * Maintainer  : Dominic Williams <dominic@mirrorx.com>, Enzo Haussecker <enzo@mirrorx.com>
 * Stability   : Experimental
 * Portability : Non-portable (requires Tox core at commit fc54980)
 *
 * This module provides a test suite for the high-level API that allows clients
 * to communicate using the Tox protocol.
 */

package libtox

import (
    "encoding/hex"
    "golang.org/x/crypto/curve25519"
    "log"
    "math/rand"
    "sync"
    "testing"
    "time"
)

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// MEMORY TESTS /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

func TestSetGetNoSpam(test *testing.T) {
    tox := initialize(test)
    defer tox.Destroy()
    noise := rand.New(rand.NewSource(time.Now().UnixNano()))
    input := noise.Uint32()
    tox.SetNoSpam(input)
    output := tox.GetNoSpam()
    if input != output {
        test.Fatalf("Failed memory test for Tox no-spam value.")
    }
}

func TestSetGetName(test *testing.T) {
    tox := initialize(test)
    defer tox.Destroy()
    noise := rand.New(rand.NewSource(time.Now().UnixNano()))
    input := make([]byte, noise.Intn(ToxMaxNameLength+1))
    for i := range input {
        input[i] = byte(noise.Intn(256))
    }
    err := tox.SetName(input)
    if err != nil {
        test.Fatal(err)
    }
    output := tox.GetName()
    if !equal(input, output) {
        test.Fatalf("Failed memory test for Tox name.")
    }
}

func TestSetGetStatus(test *testing.T) {
    tox := initialize(test)
    defer tox.Destroy()
    noise := rand.New(rand.NewSource(time.Now().UnixNano()))
    var input ToxUserStatus
    switch noise.Intn(3) {
        case 0: input = ToxUserStatusNone
        case 1: input = ToxUserStatusAway
        case 2: input = ToxUserStatusBusy
    }
    tox.SetStatus(input)
    output := tox.GetStatus()
    if input != output {
        test.Fatalf("Failed memory test for Tox status.")
    }
}

func TestSetGetStatusMessage(test *testing.T) {
    tox := initialize(test)
    defer tox.Destroy()
    noise := rand.New(rand.NewSource(time.Now().UnixNano()))
    input := make([]byte, noise.Intn(ToxMaxStatusMessageLength+1))
    for i := range input {
        input[i] = byte(noise.Intn(256))
    }
    err := tox.SetStatusMessage(input)
    if err != nil {
        test.Fatal(err)
    }
    output := tox.GetStatusMessage()
    if !equal(input, output) {
        test.Fatalf("Failed memory test for Tox status message.")
    }
}

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// CRYPTO TESTS /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

func TestPublicPrivateKeys(test *testing.T) {
    tox := initialize(test)
    defer tox.Destroy()
    publicKey := tox.GetPublicKey()
    secretKey := tox.GetSecretKey()
    var product [32]byte
    curve25519.ScalarBaseMult(&product, (*[32]byte)(&secretKey))
    if product != publicKey {
        test.Fatalf("Failed to generate valid Tox public/private key pair.")
    }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// PROTOCOL TESTS ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

func TestFriendRequest(test *testing.T) {
    // This client will send the friend request.
    tox1 := initialize(test)
    defer tox1.Destroy()
    // This client will receive and accept the friend request.
    tox2 := initialize(test)
    defer tox2.Destroy()
    // Define the friend request parameters.
    address := tox2.GetAddress()
    noise   := rand.New(rand.NewSource(time.Now().UnixNano()))
    message := make([]byte, noise.Intn(ToxMaxFriendRequestLength+1))
    // Initialize the friend request message with random data.
    for i := range message {
        message[i] = byte(noise.Intn(256))
    }
    // This function will report the connection status of the clients.
    reportConnectionStatus := func(client string, connectionStatus ToxConnectionStatus) {
        var report string
        switch connectionStatus {
            case ToxConnectionNone:
                report = "Offline"
            case ToxConnectionTCP:
                report = "Online (TCP Connection)"
            case ToxConnectionUDP:
                report = "Online (UDP Connection)"
        }
        log.Println(client + ": " + report)
    }
    // This channel is used to gracefully interrupt the main event processing
    // loops.
    done := make(chan bool, 2)
    // This channel is used to notify both clients that their time has expired.
    // It interrupts the main event processing loops, and throws a fatal error
    // in addition.
    timeout := make(chan bool, 2)
    // Launch a goroutine to schedule the timeout.
    go func() {
        time.Sleep(5 * time.Minute)
        timeout <- true
        timeout <- true
    }()
    // Initialize the client wait group.
    var client sync.WaitGroup
    client.Add(2)
    // Launch a goroutine to manage the first client. This client will send the
    // friend request.
    go func() {
        defer client.Done()
        // Report the initial connection status of the client.
        reportConnectionStatus("Client-1", tox1.GetConnectionStatus())
        // This callback function will report any changes to the connection
        // status.
        onConnectionStatus := func(tox *Tox, connectionStatus ToxConnectionStatus) {
            reportConnectionStatus("Client-1", connectionStatus)
        }
        // Get the shortened address of the second client.
        peer := hex.EncodeToString(address[:4])
        // Register the callback function.
        tox1.SetOnConnectionStatus(onConnectionStatus)
        // Bootstap the network connection.
        err := tox1.Bootstrap(DefaultSeedNode())
        if (err != nil) {
            test.Fatal(err)
        }
        // Run the main event processing loop.
        Processing:
        for {
            delay := tox1.ProcessingDelay()
            timer := time.NewTimer(delay)
            select {
                case <- timer.C:
                    tox1.Process()
                case <- done:
                    break Processing
                case <- timeout:
                    test.Fatalf("Process killed by timeout reaper.")
            }
            // Check if the client is connected.
            if (tox1.GetConnectionStatus() != ToxConnectionNone) {
                // Send the friend request.
                tox1.FriendAdd(address, message)
                log.Println("Client-1: Sent friend request to", peer)
                // Delay the retry to avoid spamming the logs.
                time.Sleep(5 * time.Second)
            }
        }
    }()
    // Launch a goroutine to manage the second client. This client will receive
    // and accept the friend request.
    go func() {
        defer client.Done()
        // Report the initial connection status of the client.
        reportConnectionStatus("Client-2", tox2.GetConnectionStatus())
        // This callback function will report any changes to the connection
        // status.
        onConnectionStatus := func(tox *Tox, connectionStatus ToxConnectionStatus) {
            reportConnectionStatus("Client-2", connectionStatus)
        }
        // This callback function will accept the friend request.
        onFriendRequest := func(tox *Tox, publicKey ToxPublicKey, messageReceived []byte) {
            // Report that a friend request was received.
            peer := hex.EncodeToString(publicKey[:4])
            log.Println("Client-2: Recieved friend request from", peer)
            // Check if the public key matches that of the first client.
            if publicKey != tox1.GetPublicKey() {
                test.Fatalf("Received friend request from unknown peer.")
            }
            // Check if the friend request message got corrupted.
            if !equal(message, messageReceived) {
                test.Fatalf("Received corrupt friend request message.")
            }
            // Accept the friend request.
            friendNumber, err := tox.FriendAddNoRequest(publicKey)
            if err != nil {
                test.Fatal(err)
            }
            log.Println("Client-2: Successfully added friend", friendNumber)
            // Notify threads to exit gracefully.
            done <- true
            done <- true
        }
        // Register the callback functions.
        tox2.SetOnConnectionStatus(onConnectionStatus)
        tox2.SetOnFriendRequest(onFriendRequest)
        // Bootstap the network connection.
        err := tox1.Bootstrap(DefaultSeedNode())
        if (err != nil) {
            test.Fatal(err)
        }
        // Run the main event processing loop.
        Processing:
        for {
            delay := tox2.ProcessingDelay()
            timer := time.NewTimer(delay)
            select {
                case <- timer.C:
                    tox2.Process()
                case <- done:
                    break Processing
                case <- timeout:
                    test.Fatalf("Process killed by timeout reaper.")
            }
        }
    }()
    client.Wait()
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// UTILITIES ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

func initialize(test *testing.T) (*Tox) {
    tox, err := New(nil, nil, nil)
    if err != nil {
        test.Fatal(err)
    }
    return tox 
}

func equal(a, b []byte) bool {
    if len(a) != len(b) {
        return false
    }
    for i := range a {
        if a[i] != b[i] {
            return false
        }
    }
    return true
}